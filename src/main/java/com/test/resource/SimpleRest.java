package com.test.resource;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("")
public class SimpleRest {

    @Context
    UriInfo uriInfo;

    private static Logger LOGGER = LoggerFactory.getLogger(SimpleRest.class);

    @GET
    @Path("/time")
    @Produces({ MediaType.APPLICATION_JSON })
	public Response whatTimeIsIt() {
    	LOGGER.debug("whatTimeIsIt is called");
    	
    	try {
    		String time = (new Date()).toString();    		
    		return Response.ok(time).build();
    	} catch (Exception e) {
    		LOGGER.error("exception occurred", e);
    		return Response.serverError().entity("internal service error").build();
    	}
    }
}
